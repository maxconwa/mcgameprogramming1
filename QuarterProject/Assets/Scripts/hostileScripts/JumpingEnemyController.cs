using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingEnemyController : MonoBehaviour
{

    public float jumpPower = 500;
    private float lastJump = 0;
    public float jumpInterval = 1;
    private bool grounded;
    public Rigidbody2D rb2;
    private ParticleSystem partSys;


    private Animator animator;

    void Start()
    {
        rb2.freezeRotation = true;

        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("No animator attached");
        }

        partSys = GetComponent<ParticleSystem>();
        if (partSys == null)
        {
            Debug.LogError("No partical system attached");
        }


    }

    void FixedUpdate()
    {       
        if (rb2.velocity.y == 0){ grounded = true; }
        else { grounded = false; }
        animator.SetBool("grounded", grounded);
        if (Time.time - lastJump > jumpInterval && grounded)
        {
            lastJump = Time.time;
            //Debug.Log("enemyJumped: " + Time.time);
            Vector3 jump = new Vector3(0, jumpPower, 0);
            rb2.AddForce(jump);
            animator.SetTrigger("jump");
            partSys.Play();
        }
       






    }

}
