using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolEnemyScript : MonoBehaviour
{
    public float moveSpeed;
    public int lORr = -1;
    public Rigidbody2D rb2;
    private Animator animator;
    private SpriteRenderer sr;
    private ParticleSystem partSys;


    // Start is called before the first frame update
    void Start()
    {
        rb2.freezeRotation = true;
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("No animator attached");
        }

        sr = GetComponent<SpriteRenderer>();
        if (sr == null)
        {
            Debug.LogError("No sprite renderer attached");
        }
        partSys = GetComponent<ParticleSystem>();
        if (partSys == null)
        {
            Debug.LogError("No partical system attached");
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb2.AddForce(new Vector2(moveSpeed * lORr, 0), ForceMode2D.Impulse);

        if (Mathf.Abs(rb2.velocity.x) > moveSpeed)
        {
            rb2.velocity = new Vector2(moveSpeed * lORr, rb2.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        ObjectType tCheck = collision.collider.GetComponent<ObjectType>();

        if (tCheck.type == ObjectType.ObjectTypes.wall)
        {
            lORr *= -1;
            partSys.Stop();


            var shape = partSys.shape;
            var rotation = shape.rotation;
            if (rotation.y == 90)
            {
                rotation.y = 270;
            }
            else
            {
                rotation.y = 90;
            }
            shape.rotation = rotation;

            partSys.Play();

            rb2.velocity = Vector3.zero;
            if(lORr == -1)
            {
                sr.flipX = false;
            }
            else
            {
                if (lORr == 1)
                {
                    sr.flipX = true;
                }
                
            }
            sr.flipX = lORr != -1;
           

        }
    }
}
