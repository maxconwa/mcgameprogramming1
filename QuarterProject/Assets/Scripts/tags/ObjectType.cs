using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectType : MonoBehaviour
{
    public enum ObjectTypes
    {
        wall,
        floor,
        platform,
        player,
        damaging,
        intangible,
        checkPoint
    }
    public ObjectTypes type;


    public float damage;
    public Vector3 pushback;
}
