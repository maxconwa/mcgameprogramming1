using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class pickaxeScript : MonoBehaviour
{
    public int swingSpeed;
    public Tilemap floor;
    public Tilemap wall;
    public float lifeTime = 0.5f;
    private float iterationLifeTime;
    public int rotation;
    public float maxDistance = 0.2f;
    // Start is called before the first frame update
    void Start()
    {

        if (lifeTime == 0)
        {
            Debug.Log("set pickaxe lifetime please");
        }
        iterationLifeTime = lifeTime;
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.parent.position, Vector3.forward, rotation * swingSpeed * Time.deltaTime);
        iterationLifeTime -= Time.deltaTime;
        if (iterationLifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }


    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("collided with something");
        ObjectType tCheck = collision.collider.GetComponent<ObjectType>();
        Debug.Log("" + tCheck);

        if (tCheck.type == ObjectType.ObjectTypes.floor || tCheck.type == ObjectType.ObjectTypes.wall)
        {
            Debug.Log("pickaxe collided");

            Vector3 worldCoords = transform.position;
            Vector3Int wallTileCoords = wall.WorldToCell(worldCoords);
            Vector3Int floorTileCoords = floor.WorldToCell(worldCoords);

            if (wall.HasTile(wallTileCoords))
            {
                Debug.Log("trying to destroy wall at tile coordinate " + wallTileCoords);
                wall.SetTile(wallTileCoords, null);
            }
            if (floor.HasTile(floorTileCoords))
            {
                Debug.Log("trying to destroy floor at tile coordinate " + floorTileCoords);
                floor.SetTile(floorTileCoords, null);
            }

        }

    }*/
}
