using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileScript : MonoBehaviour
{
    /*public static int maxProjectiles = 5;
    private static projectileScript[] projectileArray = new projectileScript[maxProjectiles];
    private static int projectilePointer = 0;*/

    public float moveSpeed;
    public int lORr = 1;
    public Rigidbody2D rb2;
    // Start is called before the first frame update
    void Start()
    {
        rb2.freezeRotation = true;

    }

    void Update()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb2.velocity = new Vector3(moveSpeed * lORr, 0, 0);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("collided");
        ObjectType tCheck = collision.collider.GetComponent<ObjectType>();

        if (tCheck.type == ObjectType.ObjectTypes.floor || tCheck.type == ObjectType.ObjectTypes.wall)
        {
            rb2.velocity = Vector3.zero;
            lORr *= -1;

        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        ObjectType tCheck = other.GetComponent<ObjectType>();
        if (tCheck.type == ObjectType.ObjectTypes.player)
        {
			GetComponent<BoxCollider2D>().isTrigger = false;
			//Debug.Log("no longer trigger");
        }
    }
}
