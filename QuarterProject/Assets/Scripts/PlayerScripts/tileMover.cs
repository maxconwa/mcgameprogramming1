using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class tileMover : MonoBehaviour
{
    public Tilemap floor;
    public Tilemap wall;
    TileBase nextTile;
    bool useFloor;
    bool useWall;
    private void Start()
    {
        nextTile = null;
        useFloor = false;
        useWall = false;
    }
    public void moveBlock(Vector3 mousePosition)
    {

        Vector3 worldPos = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector3Int wallTileCoords = wall.WorldToCell(worldPos);
        Vector3Int floorTileCoords = floor.WorldToCell(worldPos);
        if (nextTile == null)
        {
            if (wall.HasTile(wallTileCoords))
            {
                nextTile = wall.GetTile(wallTileCoords);
                wall.SetTile(wallTileCoords, null);
                useWall = true;
                useFloor = false;
                return;
            }
            if (floor.HasTile(floorTileCoords))
            {
                nextTile = floor.GetTile(floorTileCoords);
                floor.SetTile(floorTileCoords, null);
                useWall = false;
                useFloor = true;
                return;
            }
        }
        else
        {
            if (useWall && !useFloor)
            {
                wall.SetTile(wallTileCoords, nextTile);
                nextTile = null;
                useWall = false;
                useFloor = false;
            }
            if (useFloor && !useWall)
            {
                floor.SetTile(floorTileCoords, nextTile);
                nextTile = null;
                useWall = false;
                useFloor = false;
            }
        }

    }
}
