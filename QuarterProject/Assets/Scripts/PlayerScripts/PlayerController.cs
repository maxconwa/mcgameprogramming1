using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject projectile;
    public GameObject grapplingHook;
    public GameObject activeGrapplingHook = null;

    private float facing;
    public float moveSpeed = 5;
    public float jumpPower = 10;
    private int WallJumps;
    private int DefaultWallJumps = 1;
    private int WallJumpsPowerup = 0;
    private bool jump = false;
    public Rigidbody2D rb2;
    private Vector3 input;
    PlayerCollisions c;
    public Vector2 terminalVelocity;
    private ParticleSystem p;
    private AudioSource audioSorce;


    private SpriteRenderer sr;
    private Animator animator;
    private bool facingRight = false;
    public bool climbing = false;
    public bool followGh = false;
    public bool towardsOrDelete;
    public bool firedGh = false;
    public bool collided = false;

    public GameObject pickaxe;

    gridScript gS;




    // Start is called before the first frame update
    void Start()
    {

        Grid g = GameObject.FindObjectOfType<Grid>();
        gS = g.GetComponent<gridScript>();

        rb2.freezeRotation = true;
        c = gameObject.GetComponent<PlayerCollisions>();

        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("No animator attached");
        }

        sr = GetComponent<SpriteRenderer>();
        if (sr == null)
        {
            Debug.LogError("No sprite renderer attached");
        }

        p = GetComponent<ParticleSystem>();
        if (p == null)
        {
            Debug.LogError("No partical system attached");
        }
        audioSorce = GetComponent<AudioSource>();
        if (audioSorce == null)
        {
            Debug.LogError("No partical system attached");
        }

    }

    // Update is called once per frame

    void Update()
    {

        input = Vector3.zero;
        if (Input.GetKey("a")) { input.x -= 1; }
        if (Input.GetKey("d")) { input.x += 1; }
        if (Input.GetKeyDown("e")) { fireProjectile(); }
        if (Input.GetKeyDown("q"))
        {
            if (activeGrapplingHook == null && firedGh == false)
            {
                fireGrapplingHook();
                towardsOrDelete = true;
                firedGh = true;

            }
            else
            {
                if (towardsOrDelete == true && collided)
                {
                    followGh = true;
                    climbing = true;
                    towardsOrDelete = false;
                }
                else
                {

                    destroyGrapplingHook();
                    followGh = false;
                    climbing = false;
                    firedGh = false;

                }






            }

        }
        if (Input.GetKeyUp("q"))
        {
            followGh = false;
            if (GetComponent<SpringJoint2D>() != null && isGrounded())
            {
                GetComponent<SpringJoint2D>().frequency = 1;
            }
        }
        if (Input.GetMouseButtonDown(0))
        {

            Vector2 mouse = Input.mousePosition;
            Vector2 worldPosition = Camera.main.ScreenToWorldPoint(mouse);



            //https://www.codegrepper.com/code-examples/csharp/unity+2d+look+at+mousehttps://answers.unity.com/questions/798707/2d-look-at-mouse-position-z-rotation-c.html
            //https://answers.unity.com/questions/10615/rotate-objectweapon-towards-mouse-cursor-2d.html
            Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            difference.z = 0;
            difference.Normalize();
            difference *= 0.2f;


            GameObject pa = Instantiate(pickaxe, transform.position + difference, Quaternion.identity, transform);
            pickaxeScript paS = pa.GetComponent<pickaxeScript>();

            paS.rotation = worldPosition.x > transform.position.x ? -1 : 1;

            paS.floor = gS.floor;
            paS.wall = gS.wall;

        }
        if (Input.GetKeyDown("left shift"))
        {
            tileMover t = GetComponent<tileMover>();
            t.floor = gS.floor;
            t.wall = gS.wall;
            t.moveBlock(Input.mousePosition);


        }
        if (Input.GetKeyDown("space") && (isGrounded() || canWallJump()))
        {

            if (!isGrounded() && WallJumps > 0)
            {
                WallJumps--;
                jump = true;
            }
            else
            {
                if (isGrounded())
                {
                    WallJumps = DefaultWallJumps + WallJumpsPowerup;
                    jump = true;
                }


            }
        }
        if (input.x != 0)
        {
            float pre = facing;
            facing = Mathf.Sign(input.normalized.x);
            if (pre != facing)
            {
                p.Stop();


                rb2.velocity.Set(0, rb2.velocity.y);
                var shape = p.shape;
                var rotation = shape.rotation;
                if (rotation.y == 90)
                {
                    rotation.y = 270;
                }
                else
                {
                    rotation.y = 90;
                }
                shape.rotation = rotation;

                p.Play();

            }
        }
        //Debug.Log("grounded " + grounded);
        if (facing == 1)
        {
            facingRight = true;
        }
        else
        {
            facingRight = false;
        }
        //Debug.Log(facing);
        if (gameObject.GetComponent<SpringJoint2D>() != null && gameObject.GetComponent<SpringJoint2D>().connectedBody == null)
        {
            destroyGrapplingHook();
            followGh = false;
            climbing = false;
            firedGh = false;
        }


        updateAnimation();

    }

    private void FixedUpdate()
    {

        rb2.AddForce(new Vector2(moveSpeed * Time.deltaTime * input.x, 0), ForceMode2D.Impulse);
        //rb2.velocity = ;
        if (jump)
        {
            rb2.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse);
            //rb2.velocity = new Vector2(rb2.velocity.x, jumpPower);
            jump = false;
            audioSorce.Play();
        }
        if (followGh)
        {
            followGrapplingHook();
        }
        if (Mathf.Abs(rb2.velocity.y) > terminalVelocity.y)
        {
            rb2.velocity = new Vector2(rb2.velocity.x, terminalVelocity.y * Mathf.Sign(rb2.velocity.y));
        }
        if (Mathf.Abs(rb2.velocity.x) > terminalVelocity.x)
        {
            rb2.velocity = new Vector2(terminalVelocity.x * Mathf.Sign(rb2.velocity.x), rb2.velocity.y);
        }


    }

    private void updateAnimation()
    {
        animator.SetBool("grounded", isGrounded());
        if (Mathf.Abs(rb2.velocity.x) > 0)
        {
            animator.SetBool("running", true);

            sr.flipX = facingRight;
        }
        else
        {
            animator.SetBool("running", false);
        }

        if (rb2.velocity.y > 0)
        {
            animator.SetBool("rising", true);

        }
        else
        {
            animator.SetBool("rising", false);

        }

        if (rb2.velocity.y < 0)
        {
            animator.SetBool("falling", true);

        }
        else
        {
            animator.SetBool("falling", false);

        }
        animator.SetBool("climbing", climbing);

    }

    //instantiate projectile
    private void fireProjectile()
    {
        GameObject o = Instantiate(projectile, transform.position, Quaternion.identity, null);
        projectileScript p = o.GetComponent<projectileScript>();
        p.lORr = (int)facing;
    }

    private void fireGrapplingHook()
    {

        activeGrapplingHook = Instantiate(grapplingHook, transform.position, Quaternion.identity, null);
        grapplingHookScript g = activeGrapplingHook.GetComponent<grapplingHookScript>();
        g.origin = gameObject;
        g.originScript = this;

        //Debug.Log("created gh");
        Vector2 mouse = Input.mousePosition;
        Vector2 worldPosition = Camera.main.ScreenToWorldPoint(mouse);



        //https://www.codegrepper.com/code-examples/csharp/unity+2d+look+at+mousehttps://answers.unity.com/questions/798707/2d-look-at-mouse-position-z-rotation-c.html
        //https://answers.unity.com/questions/10615/rotate-objectweapon-towards-mouse-cursor-2d.html
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        difference.Normalize();

        worldPosition.Normalize();

        g.rb2.AddForce(difference * g.speed, ForceMode2D.Impulse);


        activeGrapplingHook.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));


    }

    private void followGrapplingHook()
    {
        /* SliderJoint2D j = gameObject.GetComponent<SliderJoint2D>();
        if (j == null)
        {
            Debug.Log("no slider joint to follow");
        }
        else
        {
            j.useMotor = true;
            JointMotor2D m = j.motor;
            m.motorSpeed = 1f * -1;
            j.motor = m;    
        }
       */
        SpringJoint2D j = gameObject.GetComponent<SpringJoint2D>();
        j.frequency = 0;
        if (j == null)
        {
            Debug.Log("No spring joint to follow");
        }

        j.distance -= j.distance / 15;// 2;




    }

    private void destroyGrapplingHook()
    {
        //Debug.Log("destroyed gh");

        Destroy(activeGrapplingHook);
        //SliderJoint2D j = gameObject.GetComponent<SliderJoint2D>();
        SpringJoint2D sj = gameObject.GetComponent<SpringJoint2D>();
        if (sj != null)
        {
            Destroy(sj);

        }
        collided = false;
        activeGrapplingHook = null;
    }

    private bool isGrounded()
    {
        return c.grounded;
    }

    private bool canWallJump()
    {
        return (c.touchingWall && !c.grounded) && WallJumps > 0; ;
    }




}
