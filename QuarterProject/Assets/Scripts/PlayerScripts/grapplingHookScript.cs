using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grapplingHookScript : MonoBehaviour
{

    public Rigidbody2D rb2;
    public GameObject origin;
    public PlayerController originScript;
    public float speed = 20;
    public Vector3 direction;
    LineRenderer lr;
    private bool drawLine = false;
    public Color color;
    public GameObject pivot;
    private GameObject p;
    private ParticleSystem partSys;



    public Sprite deathSprite;
    private float maxDistance = 20f;

    bool flag;
    // Start is called before the first frame update
    void Start()
    {
        flag = true;
        lr = GetComponent<LineRenderer>();
        lr.startColor = color;
        lr.endColor = color;
        lr.transform.position = new Vector3(lr.transform.position.x, lr.transform.position.y, lr.transform.position.z + 1);
        rb2 = GetComponent<Rigidbody2D>();

        partSys = GetComponent<ParticleSystem>();
        if (partSys == null)
        {
            Debug.LogError("No partical system attached");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if ((Vector3.Distance(origin.transform.position, transform.position) > maxDistance + 1) && flag)
        {
            Destroy(gameObject);

        }
    }

    private void FixedUpdate()
    {

        if (drawLine)
        {
            lr.SetPosition(0, origin.transform.position);
            lr.SetPosition(1, transform.position);
        }


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ObjectType tCheck = collision.collider.GetComponent<ObjectType>();



        if ((tCheck.type == ObjectType.ObjectTypes.floor ||
            tCheck.type == ObjectType.ObjectTypes.wall ||
            tCheck.type == ObjectType.ObjectTypes.platform) && p == null)
        {
            flag = false;
            originScript.collided = true;
            rb2.velocity = Vector3.zero;
            partSys.Play();

            SpringJoint2D sj = origin.AddComponent<SpringJoint2D>();
            sj.connectedBody = rb2;
            sj.frequency = 1;
            sj.dampingRatio = 0;

            rb2.bodyType = RigidbodyType2D.Static;


            /*p = Instantiate(pivot, transform.position, Quaternion.identity, null);
            pivotScript pScript = p.GetComponent<pivotScript>();
            pScript.medium = gameObject;
            pScript.weight = origin;
            pScript.delayedStart();*/







            drawLine = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        ObjectType tCheck = other.GetComponent<ObjectType>();
        if (tCheck.type == ObjectType.ObjectTypes.player)
        {
            GetComponent<CircleCollider2D>().isTrigger = false;
            //Debug.Log("no longer trigger");
        }
    }

    private void OnDestroy()
    {
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        sr.sprite = deathSprite;
        Destroy(p);
        p = null;




    }
}
