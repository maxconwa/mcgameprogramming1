using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerCollisions : MonoBehaviour
{

    public bool grounded = false;
    public float invincibleTime = 0;
    public float iSeconds = 5;
    public bool touchingWall = false;
    private Rigidbody2D rb2;


    private void Start()
    {
        rb2 = gameObject.GetComponent<Rigidbody2D>();
        if (rb2 == null)
        {
            Debug.Log("no rigid body attached");
        }
    }

    private void Update()
    {
        immune();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        ObjectType tCheck = collision.collider.GetComponent<ObjectType>();

        if (tCheck.type == ObjectType.ObjectTypes.floor || tCheck.type == ObjectType.ObjectTypes.platform)
        {
            grounded = false;

        }
        if (tCheck.type == ObjectType.ObjectTypes.wall)
        {
            touchingWall = false;

        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        GoalScript gCheck = collision.GetComponent<GoalScript>();

        if (gCheck != null)
        {
            gCheck.win();

        }
        //Debug.Log($"Touched {collision}");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ObjectType tCheck = collision.collider.GetComponent<ObjectType>();

        if (tCheck.type == ObjectType.ObjectTypes.floor || tCheck.type == ObjectType.ObjectTypes.platform)
        {
            grounded = true;

        }
        if (tCheck.type == ObjectType.ObjectTypes.wall)
        {
            touchingWall = true;


        }

        //null if doesnt exist
        //needs rigid body to interact with triggers
        if (tCheck.type == ObjectType.ObjectTypes.damaging)
        {
            Debug.Log("collided with damaging");
            if (invincibleTime > 0) { return; }
            Debug.Log("invincible time is greater than 0");
            GetComponent<HeartSystem>().TakeDamage();
            Debug.Log("take damage called");
            /*Vector3 otherPos = collision.transform.position;
            //transform.position += Vector3.up * 2;
            Vector3 force = tCheck.pushback;
            if (otherPos.x > transform.position.x)
            {
                force.x *= -1;

            }
            gameObject.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
            invincibleTime = iSeconds;*/
        }

    }

    //check if in I frames
    void immune()
    {
        invincibleTime -= Time.deltaTime;
        if (invincibleTime > 0)
        {
            Color c = GetComponent<SpriteRenderer>().color;
            c.a = (c.a == 255f) ? c.a = 100f : c.a = 255f;
        }
        else
        {
            GetComponent<SpriteRenderer>().enabled = true;

        }

    }
}
