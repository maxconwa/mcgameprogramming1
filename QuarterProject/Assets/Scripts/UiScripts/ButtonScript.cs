using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ButtonScript : MonoBehaviour
{
    public enum ButtonType
    {
        newGame,
        resume,
        restart,
        menu,
        infinite,
        NULL
    }
    public ButtonType buttonType;
    GameManagerScript gameManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        gameManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
        if (gameManagerScript == null)
        {
            Debug.Log("please set up game manager");
        }
        Text t = GetComponentInChildren<Text>();

        switch (buttonType)
        {
            case ButtonType.newGame:
                t.text = "New Game";
                break;
            case ButtonType.resume:
                t.text = "Resume";
                break;
            case ButtonType.menu:
                t.text = "Menu";
                break;
            case ButtonType.NULL:
                t.text = "null";
                break;
            case ButtonType.infinite:
                t.text = "infinite";
                break;


        }
    }

    public void call()
    {
        switch (buttonType)
        {
            case ButtonType.newGame:
                SceneManager.LoadScene(gameManagerScript.firstScene);
                break;
            case ButtonType.resume:
                SceneManager.LoadScene(gameManagerScript.currentScene);
                break;
            case ButtonType.restart:
                SceneManager.LoadScene(gameManagerScript.currentScene);
                break;
            case ButtonType.menu:
                SceneManager.LoadScene(gameManagerScript.menu);
                break;
            case ButtonType.NULL:
                Debug.Log("button of type null");
                break;
            case ButtonType.infinite:
                SceneManager.LoadScene("generationTest");
                break;
        }
    }
}
