﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;




public class SidescrollController : CharacterController2D
{
   

    [Header("Settings")]
	public float speed = 7;
	public float gravity = 33;
	public float jumpPower = 15;
	public float friction = 255;
	public float terminalVelocity = 40;
	public float skinWidth = .01f / 16f;
	public Vector3 velocity;

	bool isGrounded;
	float facing;
	Vector3 input, movement, moved;

	
	float invincibleTime = 0;
	public GameObject projectile;


	public float iSeconds = 5;

    private void Start()
    {
		Physics2D.queriesStartInColliders = false;
	}
	/// <summary> See if we are currently grounded.</summary>
	public bool CheckGrounded() { return velocity.y <= 0 && IsTouching(Vector2.down * skinWidth); }
	/// <summary> Check if we will touch the ground during the next frame </summary>
	public bool CheckWillTouchGround() { return velocity.y <= 0 && IsTouching(new Vector2(0, velocity.y * Time.deltaTime)); }


	/*public float rayLength = 0.0625f;
    private void FixedUpdate()
    {
		Ray2D ray = new Ray2D();
		ray.origin = transform.position;
		ray.direction = moved * rayLength;
		Debug.DrawRay(ray.origin, ray.direction, Color.red, 5.0f);
		RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, rayLength);
        if(hit == false) { return; }
		Debug.Log("got a hit?" + hit.collider.gameObject.name);

		Damaging dCheck = hit.collider.GetComponent<Damaging>();
		if (dCheck != null)
		{
			if (invincibleTime > 0) { return; }
			GetComponent<HeartSystem>().TakeDamage();
			Vector3 otherPos = hit.transform.position;
			transform.position += Vector3.up * skinWidth * 2;
			Vector3 force = dCheck.pushback;
			if (otherPos.x > transform.position.x)
			{
				force.x *= -1;

			}
			AddForce(force);
			invincibleTime = iSeconds;
		}

	}*/
    //input handling and calling other functions
    void Update()
	{
		if(Time.deltaTime > 0.1f) { return;  }
		input = Vector3.zero;

		if (Input.GetKey("a")) { input.x -= 1; }
		if (Input.GetKey("d")) { input.x += 1; }
		if (Input.GetKeyDown("e")) { fireProjectile(); }

		input = input.normalized;
		if (input.x != 0)
		{
			facing = Mathf.Sign(input.x);
		}

		if (isGrounded)
		{
			velocity.y = 0;
			if (Input.GetButtonDown("Jump"))
			{
				velocity.y = jumpPower;
			}
			velocity.x = Mathf.MoveTowards(velocity.x, 0, friction * Time.deltaTime);
		}
		else
		{
			velocity.y -= gravity * Time.deltaTime;
		}

		if (velocity.y > 0 && Input.GetButtonUp("Jump"))
		{
			velocity.y = 0;
		}
		if (velocity.y < -terminalVelocity) { velocity.y = -terminalVelocity; }
		movement = input * speed;
		movement += velocity;

		moved = Move(movement * Time.deltaTime);
		if (moved.x == 0 && velocity.x != 0) { velocity.x *= .5f; }
		if (velocity.y < 0 && CheckWillTouchGround()) { velocity.y *= .5f; }
		isGrounded = CheckGrounded();


		immune();


	}

	public void AddForce(Vector3 force)
    {
		velocity += force;
    }
	private void OnCollisionEnter2D(Collision2D collision)
	{ 
		Debug.Log("collided with someting");
		ObjectType tCheck = collision.collider.GetComponent<ObjectType>();
		//null if doesnt exist
		//needs rigid body to interact with triggers
		if (tCheck.type == ObjectType.ObjectTypes.damaging)
		{
			if (invincibleTime > 0) { return; }
			GetComponent<HeartSystem>().TakeDamage();
			Vector3 otherPos = collision.transform.position;
			transform.position += Vector3.up * skinWidth * 2;
			Vector3 force = tCheck.pushback;
			if (otherPos.x > transform.position.x)
			{
				force.x *= -1;

			}
			AddForce(force);
			invincibleTime = iSeconds;
		}
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		
		GoalScript gCheck = collision.GetComponent<GoalScript>();

		if (gCheck != null)
		{
			SceneManager.LoadScene("WIN");
		}
		Debug.Log($"Touched {collision}");
	}

	
	//instantiate projectile
	public void fireProjectile()
	{
		GameObject o = Instantiate(projectile, transform.position, Quaternion.identity, null);
		projectileScript p = o.GetComponent<projectileScript>();
		//p.velocity.x *= facing;
	}
	//check if in I frames
	void immune()
	{
		invincibleTime -= Time.deltaTime;
		if (invincibleTime > 0)
        {
			GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
		} else
        {
			GetComponent<SpriteRenderer>().enabled = true;

		}

	}
}
