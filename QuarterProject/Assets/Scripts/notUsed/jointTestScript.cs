using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jointTestScript : MonoBehaviour
{
    public GameObject pivot;
    public GameObject medium;
    public GameObject weight;
    // Start is called before the first frame update
    

    void delayedStart()
    {
        pivot.AddComponent<BoxCollider2D>();
        medium.AddComponent<BoxCollider2D>();
        weight.AddComponent<BoxCollider2D>();

        pivot.AddComponent<Rigidbody2D>();
        medium.AddComponent<Rigidbody2D>();
        weight.AddComponent<Rigidbody2D>();


        Rigidbody2D pivotRb = pivot.GetComponent<Rigidbody2D>();
        Rigidbody2D mediumRb = medium.GetComponent<Rigidbody2D>();
        Rigidbody2D weightRb = weight.GetComponent<Rigidbody2D>();

        pivotRb.bodyType = RigidbodyType2D.Static;
        HingeJoint2D hj = pivot.AddComponent<HingeJoint2D>();
        hj.connectedBody = mediumRb;

        SliderJoint2D sj = weight.AddComponent<SliderJoint2D>();
        sj.connectedBody = mediumRb;
        JointTranslationLimits2D l = sj.limits;
        l.max = 5;
        sj.limits = l;
        sj.useLimits = true;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
