using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pivotScript : MonoBehaviour
{

    public GameObject medium = null;
    public GameObject weight = null;
    private SpringJoint2D sj;

    public void delayedStart()
    {
        

            gameObject.AddComponent<BoxCollider2D>();
            Rigidbody2D pivotRb = gameObject.GetComponent<Rigidbody2D>();
            if(pivotRb == null)
            {
                pivotRb = gameObject.AddComponent<Rigidbody2D>();
            }

            pivotRb.bodyType = RigidbodyType2D.Static;



            Rigidbody2D mediumRb = medium.GetComponent<Rigidbody2D>();
            Rigidbody2D weightRb = weight.GetComponent<Rigidbody2D>();

            sj = weight.AddComponent<SpringJoint2D>();
            sj.connectedBody = mediumRb;
            mediumRb.bodyType = RigidbodyType2D.Static;
            //HingeJoint2D hj = gameObject.AddComponent<HingeJoint2D>();
            //hj.connectedBody = weightRb;


            //SliderJoint2D sj = weight.AddComponent<SliderJoint2D>();
            //sj.connectedBody = mediumRb;

            //JointTranslationLimits2D l = sj.limits;
            //l.max = Vector3.Distance(mediumRb.position, weightRb.position);
            //sj.limits = l;
            //sj.useLimits = true;
        
    }
    private void OnDestroy()
    {
        Destroy(sj);
    }


}
