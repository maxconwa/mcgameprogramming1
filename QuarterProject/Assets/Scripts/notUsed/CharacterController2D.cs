﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class CharacterController2D : MonoBehaviour {

	public Collider2D col;
	public float snapDistance = 1f/16f;

	protected Collider2D[] collisions = new Collider2D[16];
	protected RaycastHit2D[] raycastHits = new RaycastHit2D[16];

	/// <summary> Attempt to move the object, and get how far they actually moved. </summary>
	/// <returns> Applied movement </returns>
	public Vector3 Move(Vector3 movement, Func<Vector3, Vector3> checkType = null) {
		if(checkType == null)
        {
			checkType = DoMove;
        }

        col = GetComponent<Collider2D>();
		//Physics.BoxCastNonAlloc()
		Vector3 moved = Vector3.zero;
		moved += checkType(new Vector3(0, movement.y, 0));
		while (moved.y == 0 && Math.Abs(movement.y) > snapDistance) {
			movement.y *= .5f;
			moved += checkType(new Vector3(0, movement.y, 0));
		}
		moved += checkType(new Vector3(movement.x, 0, 0));
		while (moved.x == 0 && Math.Abs(movement.x) > snapDistance) {
			movement.x *= .5f;
			moved += checkType(new Vector3(movement.x, 0, 0));
		}
		return moved;
	}
  



    /// Attempt to move the object, and get how far they actually moved. 
    /// <param name="movement"> Requested movement. </param>
    /// <returns> Applied movement </returns>
    public Vector3 DoMove(Vector3 movement) {
		bool move = true;

		if (col != null) {
			if (col is BoxCollider2D) {
				BoxCollider2D box = col as BoxCollider2D;
				Vector3 point = box.transform.position + (Vector3)box.offset + movement;
				int numCollisions = Physics2D.OverlapBoxNonAlloc(point, box.size, 0, collisions);
				if (numCollisions != 0) {
					for (int i = 0; i < numCollisions; i++) {
						if (collisions[i] == col) { continue; }
						if (!collisions[i].isTrigger) {
							move = false;
						} 

					}
				}
			}

		}

		if (move) {
			transform.position = transform.position + movement;

			return movement;
		}

		return Vector3.zero;
	}


	/// Check if the object would touch something when their collider is swept along a direction
	/// Sweep direction/distance vector
	/// True if they would hit something, false otherwise.
	public bool IsTouching(Vector3 sweep) {
		if (col != null) {
			if (col is BoxCollider2D) {
				BoxCollider2D box = col as BoxCollider2D;
				Vector3 point = box.transform.position + (Vector3)box.offset;
				//float adjWidth = 1f;

				Vector2 adjSize = box.size;
				//adjSize.x *= adjWidth;

				int numCollisions = Physics2D.BoxCastNonAlloc(point, adjSize, 0, sweep, raycastHits, sweep.magnitude + snapDistance);
				if (numCollisions > 0) {
					int lowest = -1;
					float lowestDistance = sweep.magnitude + snapDistance;

					for (int i = 0; i < numCollisions; i++) {
						if (raycastHits[i].collider == col) { continue; } // Skip own collider.
						if (!raycastHits[i].collider.isTrigger) {

							if (raycastHits[i].distance < lowestDistance) {
								lowest = i;
								lowestDistance = raycastHits[i].distance;
							}

						}
					}

					if (lowest >= 0) {
						return true;
					}

				}
			}
		}

		return false;
	}

 
}
