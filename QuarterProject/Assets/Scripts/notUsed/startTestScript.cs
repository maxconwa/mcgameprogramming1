using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startTestScript : MonoBehaviour
{
    // Start is called before the first frame update
    public pivotScript p;
    void Start()
    {
        if(p == null)
        {
            Debug.Log("no script to call");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Debug.Log("spacePressed");
            p.delayedStart();

        }
    }
}
