using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class inversePickaxe : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {

        Tilemap tm = GetComponent<Tilemap>();
        Vector3 worldCoords = collision.transform.position;
        Vector3Int tileCoords = tm.WorldToCell(worldCoords);

        Debug.Log("name: " + gameObject.name);
        Debug.Log("worldCoords: " + worldCoords);
        Debug.Log("tileCoords: " + tileCoords);
        Debug.Log("tileCoords to world cords: " + tm.CellToWorld(tileCoords));

        if (tm.HasTile(tileCoords))
        {
            Debug.Log("trying to destroy wall at tile coordinate " + tileCoords);
            tm.SetTile(tileCoords, null);
        }
    }
}