using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    public string firstScene = "level1"; //resume
    public string currentScene; //restart
    public string nextScene; //restart

    public string menu = "intro";

    // Start is called before the first frame update
    void Start()
    {
     if(currentScene == null)
        {
            Debug.Log("no current scene given");
            currentScene = "level1";
        }
     if(nextScene == null)
        {
            Debug.Log("no next scene given");
            nextScene = "level1";
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
