using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GoalScript : MonoBehaviour
{
    public AudioSource audioSource;
    

    public void win()
    {
        audioSource.Play();
        GetComponent<ParticleSystem>().Play();
        //yield return new WaitForSeconds(1);
        GameManagerScript gameManagerScript = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerScript>();
        SceneManager.LoadScene(gameManagerScript.nextScene);
    }
    

}
