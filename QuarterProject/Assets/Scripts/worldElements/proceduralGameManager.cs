using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

public class proceduralGameManager : MonoBehaviour
{

    public int maxCircleRadius;
    public int rowColumnSegments = 5;
    public Grid grid;
    public Tilemap floor;
    public Tilemap wall;
    public BoundsInt area;
    public TileBase[] fillings;
    public TileBase[] toppings;
    public float luck;
    public int maxRadius = 10;
    public GameObject player;
    private GameObject p;


    private Coroutine c;




    void Start()
    {
        if (player == null)
        {
            Debug.Log("no player found");
        }
        if (rowColumnSegments == 0)
        {
            Debug.Log("row col segments cannot be 0");
            rowColumnSegments = 5;
        }
        if (maxCircleRadius == 0)
        {
            Debug.Log("maxCircleRadius cannot be 0");
            maxCircleRadius = 5;
        }
        if (luck == 0)
        {
            Debug.Log("luck cannot be 0");
            luck = 0.25f;
        }
        if (floor == null || wall == null || grid == null)
        {
            grid = GetComponent<Grid>();
            floor = grid.GetComponentInChildren<Tilemap>(); //specify how?
            wall = grid.GetComponentInChildren<Tilemap>();
        }
        for (int x = -5; x < area.size.x + 5; x++)
        {
            floor.SetTile(new Vector3Int(x, 0, 0), fillings[Random.Range(0, fillings.Length)]);
        }
        c = StartCoroutine(Generation());
        p = Instantiate(player, new Vector3(2, 0.18f, 0), Quaternion.identity, null);

    }

    // Update is called once per frame
    void Update()
    {
        if (p.transform.position.y > (area.size.y * 0.08) - 4)
        {
            int prevX = (int)area.size.x;
            int prevY = (int)area.size.y;
            Vector3Int s = area.size;
            s.y = prevY * 2;
            area.size = s;
            c = StartCoroutine(Generation(startHeight: prevY + rowColumnSegments));
        }




    }

    protected virtual IEnumerator Generation(int startHeight = 5)
    {
        bool debug = false;
        for (int mapX = 0; mapX < area.size.x; mapX += rowColumnSegments)
        {
            for (int mapY = startHeight; mapY < area.size.y; mapY += rowColumnSegments)
            {
                if (Random.Range(0f, 1f) < luck)
                {
                    circle(Random.Range(0, maxCircleRadius), floor, new Vector3Int(mapX, mapY, 0));
                    if (debug) Debug.Log("made circle");
                }

            }
            yield return null;
        }

    }


    void circle(int radius, Tilemap f, Vector3Int offset)
    { //https://www.geeksforgeeks.org/program-print-circle-pattern/
        double dist;
        Vector3Int origin = offset + new Vector3Int(radius, radius, 0);
        Vector3Int top = offset + new Vector3Int(radius, radius * 2, 0);
        for (int x = 0; x <= 2 * radius; x++)
        {
            for (int y = 0; y <= 2 * radius; y++)
            {
                dist = Mathf.Sqrt((x - radius) *
                        (x - radius) + (y - radius)
                                    * (y - radius));

                // dist should be in the range
                // (radius - 0.5) and (radius + 0.5)
                // to print stars(*)
                if (dist > radius - 0.5 &&
                               dist < radius + 0.5)
                {
                    if ((offset + new Vector3Int(x, y, 0)).y > origin.y)
                        floor.SetTile(offset + new Vector3Int(x, y, 0), toppings[Random.Range(0, toppings.Length)]);
                    else wall.SetTile(offset + new Vector3Int(x, y, 0), fillings[Random.Range(0, fillings.Length)]);
                }

                else
                {
                    if (dist < radius)
                    {
                        wall.SetTile(offset + new Vector3Int(x, y, 0), fillings[Random.Range(0, fillings.Length)]);
                    }
                }

            }
        }
    }
}
