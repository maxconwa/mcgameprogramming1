using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbitScript : MonoBehaviour
{
    public float orbitSpeed = 20;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //GetComponentInParent<Transform>().position
        transform.RotateAround(transform.parent.position, Vector3.forward, orbitSpeed * Time.deltaTime);
        //transform.Rotate(,);
    }
}
