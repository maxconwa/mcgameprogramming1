using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sendMessageTimer : MonoBehaviour
{
    public float duration;

    private float endTime;

    private bool timerStarted;

    public GameObject calledObject;

    // Start is called before the first frame update
    void Start()
    {
        timerStarted = false;
        StartTimer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //virtual allows object system to call the most child method in heritance
    public virtual void StartTimer()
    {
        timerStarted = true;

        //how we start a coroutine
        StartCoroutine(StartCountdown());
    }

    protected virtual IEnumerator StartCountdown()
    {
        endTime = Time.time + duration;

        while(Time.time < endTime)
        {
            calledObject.SendMessage("OnTimerTick", this);
            yield return null;
        }
        calledObject.SendMessage("OnTimerExpired");
    }

    public float timeLeft()
    {
        if(Time.time >= endTime)
        {
            return 0;
        }
        else
        {
            return endTime - Time.time;
        }
    }
}
