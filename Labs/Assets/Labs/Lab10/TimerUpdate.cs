using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUpdate : MonoBehaviour
{

    Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = gameObject.GetComponent<Text>();
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTimerTick(sendMessageTimer timer)
    {
        text.text = "" + timer.timeLeft();
    }

    public void OnTimerExpired()
    {
        text.text = "0";
    }
}
