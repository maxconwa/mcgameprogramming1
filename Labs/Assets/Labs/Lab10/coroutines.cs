using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coroutines : MonoBehaviour
{
    public Vector3 target;
    private Vector3 origin;
    float moveSpeed = 5f;


    public Color targetColor;
    private Color originColor;
    float colorChangeRate = 5f;


    private Coroutine c;
    private bool running;
    SpriteRenderer sr;
    // Start is called before the first frame update
    private void Start()
    {
        origin = transform.position;
        running = true;
        sr = GetComponent<SpriteRenderer>();
        if(sr == null)
        {
            Debug.Log("no sprite renderer attached");
        }
        originColor = sr.color;
        c = StartCoroutine(move());

    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            running = !running;
        }

     
    }



    protected virtual IEnumerator move()
    {
        while (running)
        {

            if (transform.position == target)
            {
                Vector3 tmp = origin;
                origin = target;
                target = tmp;

                Color tmpColor = originColor;
                originColor = targetColor;
                targetColor = tmpColor;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, target, moveSpeed * Time.deltaTime);
            }
            Debug.Log("sr" + sr == null);
            Debug.Log("oc" + originColor == null);
            Debug.Log("tc" + targetColor == null);
            Debug.Log("ccR" +colorChangeRate == null);
            sr.color = Color.Lerp(originColor, targetColor, colorChangeRate);

            yield return null;
        }
        

    }
    
}
