using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidBodyMoveScript : MonoBehaviour
{
    public Vector3 nextMove = Vector3.zero;
    public float maxSpeed;
    bool jump;
    int jumpForce = 50;
    int moveForce = 5;

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetKey("w"))
        {
            jump = true;
        }


    }

    /* void FixedUpdate()
     {
         Rigidbody2D r = GetComponent<Rigidbody2D>();
         r.AddForce(nextMove);
         Debug.Log(nextMove);
     }*/
    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        // if the player is changing direction (h has a different sign than 
        // velocity.x), or hasn't reached maxSpeed yet...
        Rigidbody2D rigidbody2D = GetComponent<Rigidbody2D>();
        if (h * rigidbody2D.velocity.x < maxSpeed)
        {
            rigidbody2D.AddForce(Vector2.right * h * moveForce);
        }

        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
        {
            rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);
        }

        // an example of adding jumping 
        if (jump)
        {
            rigidbody2D.AddForce(new Vector2(0f, jumpForce));
            //Debug.Log("jumped, " + (Vector2.right * h * moveForce));
            jump = false;
        }
    }
}
