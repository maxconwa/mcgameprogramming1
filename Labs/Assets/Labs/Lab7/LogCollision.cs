using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogCollision : MonoBehaviour
{
    public Collider2D col;
    private void Start()
    {
        col = GetComponent<Collider2D>();
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log(col.gameObject.name + " entered a collision with me!");
    }

    // called if two objects are overlapping
    void OnCollisionStay2D(Collision2D other)
    {
        Debug.Log(other.gameObject.name + " is in a collision with me!");
    }

    // called if the other object is leaving the collision
    void OnCollisionExit2D(Collision2D other)
    {
        Debug.Log(other.gameObject.name + " left a collision with me!");
    }
}
