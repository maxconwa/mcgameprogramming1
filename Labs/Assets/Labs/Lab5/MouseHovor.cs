using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHovor : MonoBehaviour
{
    public Camera cam;
    public Color hoverColor;
    public Vector3 mousePos;

    public Vector3 mousePosInWorldCoords;
    void Start()
    {
        if (cam == null)
        {
            Debug.LogError("Camera not deffined yet");
        }

    }


    void Update()
    {
        mousePosInWorldCoords = cam.ScreenToWorldPoint(Input.mousePosition);


        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        mousePos = Input.mousePosition;

        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.name == gameObject.name)
            {
                GetComponent<SpriteRenderer>().color = hoverColor;
            }
            else
            {
                GetComponent<SpriteRenderer>().color = Color.white;
            }
        }

        else
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }

    }
}
