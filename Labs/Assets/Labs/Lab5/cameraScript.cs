using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScript : MonoBehaviour
{
    public float speed = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = new Vector3();
        if (Input.GetKey("left"))
        {
            move += (Vector3.left * speed);
        }
        if (Input.GetKey("right"))
        {
            move += (Vector3.right * speed);
        }
        if (Input.GetKey("up"))
        {
            move += (Vector3.up * speed);
        }
        if (Input.GetKey("down"))
        {
            move += (Vector3.down * speed);
        }

        transform.Translate(move);


    }
}
