using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTFailNode : BTNode
{
    public BTFailNode(BehaviorTree t) : base(t)
    {


    }

    public override Result Execute()
    {
        return Result.Failure;
    }
}
