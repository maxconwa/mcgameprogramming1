using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTRandomWalk : BTNode
{
    protected Vector3 NextDestination { get; set; }
    public float speed = 2;

    public BTRandomWalk(BehaviorTree t): base(t)
    {
        FindNextDestination();
    }

    public override Result Execute()
    {
        // if we've made it to the destination
        if (Tree.gameObject.transform.position == NextDestination)
        {
            if (!FindNextDestination())
                return Result.Failure;

            Debug.Log("Got to destination: " + NextDestination);
            return Result.Success;
        }
        else
        {
            Debug.Log("b: " + Tree.gameObject.transform.position); 
            Tree.gameObject.transform.position =
                Vector3.MoveTowards(Tree.gameObject.transform.position,
                NextDestination, Time.deltaTime * speed);
            Debug.Log("a: " + Tree.gameObject.transform.position);
            return Result.Running;
        }

    }

    public bool FindNextDestination()
    {
        object o;
        bool found = false;
        found = Tree.Blackboard.TryGetValue("WorldBounds", out o);
        if (found)
        {
            Rect bounds = (Rect)o;
            float x = Random.value * bounds.width;
            float y = Random.value * bounds.height;
            NextDestination = new Vector3(x, y, NextDestination.z);
            Debug.Log("Next destination is: " + NextDestination);
        }

        return found;
    }
}
