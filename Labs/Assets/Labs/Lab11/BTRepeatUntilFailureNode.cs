using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTRepeatUntilFailureNode : BTComposite
{
    private int currentNode = 0;

    public BTRepeatUntilFailureNode(BehaviorTree t, BTNode[] c) : base(t, c)
    {


    }

    public override Result Execute()
    {
        while (Children[currentNode].Execute() != Result.Failure)
        {
            currentNode += 1;
            currentNode %= Children.Count;
        }
        return Children[currentNode].Execute();
    }
     
}
