using UnityEngine;
using System.Collections;

public class BTRepeater : Decorator
{

    public BTRepeater(BehaviorTree t, BTNode child) : base(t, child)
    {
    }

    public override Result Execute()
    {
        Debug.Log("Child returned: " + Child.Execute());
        return Result.Running;
    }
}