using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lab1 : MonoBehaviour
{
    public float RotationSpeed = 30;
    public float orbitSpeed = 1;
    // Start is called before the first frame update

    public Vector2 Velocity = new Vector2(1, 0);

   public float Radius = 1;
   private float _angle = 1;
   private int tick = 0;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      gameObject.transform.Rotate(0, 0, Time.deltaTime * RotationSpeed);
      _angle += orbitSpeed * Time.deltaTime;

       transform.position = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * Radius;
       Debug.Log("frame tick " + tick++);
      //gameObject.transform.position =
    }
}
//code modified from
//https://www.reddit.com/r/Unity2D/comments/34qm8v/how_to_move_an_object_in_a_circular_pattern/
