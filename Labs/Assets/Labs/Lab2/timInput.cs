using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timInput : MonoBehaviour
{
    public float maxSpeed;
    public float speed;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        //finds a component of type animator thats attached to the game object that owns the script
        animator = GetComponent<Animator>();
        if(animator == null){
            Debug.LogError("No animator attached");
        }

    }

    // Update is called once per frame
    void Update()
    {
      float horizontalAxis = Input.GetAxis("Horizontal");
      //check controller left or right
      if (Mathf.Abs(horizontalAxis) > 0){
        speed = maxSpeed * horizontalAxis * Time.deltaTime;
        transform.Translate(speed, 0.0f, 0.0f);
        animator.SetBool("isRunning", true);
      } else{
        animator.SetBool("isRunning", false);
      }

    }
}
