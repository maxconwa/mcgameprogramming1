using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


public class GameModel : MonoBehaviour
{

    public GameObject player;
    public string saveFileName;

    private static GameModel mSingleton;


    //allows us to access the instance wherever we are in code without having to look it up
    public static GameModel Instance { get { return mSingleton; } }
    //is called immediatly after constructor
    void Awake()
    {
        if (mSingleton == null)
        {
            //if singleton hasnt been set, set it now
            mSingleton = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SaveGameModel(saveGame save, string fileName)
    {
        //write objects to a stream
        BinaryFormatter bf = new BinaryFormatter();
        //create a file that we store data to
        FileStream fs = File.OpenWrite(Application.persistentDataPath + "/" + fileName + ".dat");

        save.StoreData(this);
        bf.Serialize(fs, save);

        fs.Close();

    }


    public void LoadGame(string fileName)
    {
        BinaryFormatter bf = new BinaryFormatter();

        FileStream fs = File.OpenRead(Application.persistentDataPath + "/" + fileName + ".dat");

        saveGame savegame = (saveGame)bf.Deserialize(fs);

        savegame.LoadData(this);
        fs.Close();
    }

    public void OnSaveClick()
    {
        saveGame sg = new saveGame();

        SaveGameModel(sg, saveFileName);
    }
    public void OnLoadClick()
    {
        LoadGame(saveFileName);
    }

}
