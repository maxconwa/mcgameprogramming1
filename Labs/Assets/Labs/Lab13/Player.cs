using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public int health;
    public int exp;
    public int step;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        if (Input.GetKeyDown("w"))
        {
            pos.y += step;
        }
        if (Input.GetKeyDown("a"))
        {
            pos.x -= step;
        }
        if (Input.GetKeyDown("s"))
        {
            pos.y -= step;
        }
        if (Input.GetKeyDown("d"))
        {
            pos.x += step;
        }
        transform.position = pos;
    }
}
