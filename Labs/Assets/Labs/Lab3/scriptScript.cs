using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class scriptScript : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject obj;
    gameManagerScript sm;
    public Vector3 startPosition;
    public Vector3 endPosition;
    float movementSpeed, rotationSpeed, scalingSpeed;
    void Start()
    {
      obj = GameObject.FindGameObjectWithTag ("GameManager");
      sm = obj.GetComponent<gameManagerScript> ();
      movementSpeed = sm.movementSpeed;
      rotationSpeed = sm.rotationSpeed;
      scalingSpeed = sm.scalingSpeed;
      transform.position = startPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(endPosition);
        transform.position = Vector3.MoveTowards(transform.position, endPosition, movementSpeed);
        transform.Rotate(0,0,rotationSpeed);
        transform.localScale += new Vector3(0,0,scalingSpeed);
        if(transform.position == endPosition)
        {
            Vector3 tmp = endPosition;
            endPosition = startPosition;
            startPosition = tmp;
        }
    }
}
